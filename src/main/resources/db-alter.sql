DROP TABLE IF EXISTS "person";
CREATE TABLE "person" (
"id" int4 NOT NULL,
"name" varchar(255) COLLATE "default"
);
ALTER TABLE "person" ADD PRIMARY KEY ("id");

INSERT INTO "person" ("id", "name") VALUES ('1', 'Hưng');
INSERT INTO "person" ("id", "name") VALUES ('2', 'Linh');
