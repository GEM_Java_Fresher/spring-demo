package com.gem.training.dao;

import com.gem.training.model.Person;

import java.util.List;

/**
 * Created by training on 1/19/15.
 */
public interface PersonDAO
{
    Person findByID(int id);

    List<Person> findAll();
}
