package com.gem.training.dao.impl;

import com.gem.training.dao.PersonDAO;
import com.gem.training.model.Person;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by training on 1/19/15.
 */
@Repository
public class PersonDAOImpl implements PersonDAO
{
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public Person findByID(int id)
    {
        String sql = "Select p from Person p where p.id = :id";

        Query query = entityManager.createQuery(sql).setParameter("id", id);

        return (Person) query.getSingleResult();
    }

    @Override
    public List<Person> findAll()
    {
        String sql = "Select p from Person p";

        Query query = entityManager.createQuery(sql);

        return query.getResultList();
    }
}
