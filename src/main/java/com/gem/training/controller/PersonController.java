package com.gem.training.controller;

import com.gem.training.dto.PersonDTO;
import com.gem.training.dto.PersonListDTO;
import com.gem.training.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by com.gem.training on 1/19/15.
 */
@Controller
@RequestMapping("/person")
public class PersonController
{
    @Autowired
    PersonService personService;

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public PersonDTO getPerson(@PathVariable(value = "userId") int userId)
    {
        return personService.getPersonById(userId);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public PersonListDTO findAll()
    {
        return personService.findAll();
    }

}
