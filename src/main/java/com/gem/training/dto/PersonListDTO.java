package com.gem.training.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by training on 1/20/15.
 */
public class PersonListDTO
{
    private List<PersonDTO> personDTOList = new ArrayList<PersonDTO>();

    public List<PersonDTO> getPersonDTOList()
    {
        return personDTOList;
    }

    public void setPersonDTOList(List<PersonDTO> personDTOList)
    {
        this.personDTOList = personDTOList;
    }
}
