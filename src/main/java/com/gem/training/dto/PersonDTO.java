package com.gem.training.dto;

import com.gem.training.model.Person;

/**
 * Created by training on 1/19/15.
 */
public class PersonDTO
{
    private String id;
    private String name;

    public PersonDTO(Person person)
    {
        this.id = String.valueOf(person.getId());
        this.name = person.getName();
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }


}
