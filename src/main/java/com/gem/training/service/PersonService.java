package com.gem.training.service;

import com.gem.training.dto.PersonDTO;
import com.gem.training.dto.PersonListDTO;
import org.springframework.stereotype.Service;

/**
 * Created by training on 1/19/15.
 */
@Service
public interface PersonService
{
    public PersonDTO getPersonById(int id);

    public PersonListDTO findAll();
}
