package com.gem.training.service.impl;

import com.gem.training.dao.PersonDAO;
import com.gem.training.dto.PersonDTO;
import com.gem.training.dto.PersonListDTO;
import com.gem.training.model.Person;
import com.gem.training.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by training on 1/19/15.
 */
@Service
public class PersonServiceImpl implements PersonService
{
    @Autowired
    PersonDAO personDAO;

    public PersonDTO getPersonById(int id)
    {
        Person person = personDAO.findByID(id);
        return new PersonDTO(person);
    }

    public PersonListDTO findAll()
    {
        PersonListDTO personListDTO = new PersonListDTO();
        List<Person> persons = personDAO.findAll();

        for (Person person : persons)
        {
            PersonDTO personDTO = new PersonDTO(person);
            personListDTO.getPersonDTOList().add(personDTO);
        }
        return personListDTO;
    }
}
